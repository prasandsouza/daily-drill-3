const items = [
     {
          name: "Orange",
          available: true,
          contains: "Vitamin C",
     },
     {
          name: "Mango",
          available: true,
          contains: "Vitamin K, Vitamin C",
     },
     {
          name: "Pineapple",
          available: true,
          contains: "Vitamin A",
     },
     {
          name: "Raspberry",
          available: false,
          contains: "Vitamin B, Vitamin A",
     },
     {
          name: "Grapes",
          contains: "Vitamin D",
          available: false,
     },
];

// 1. Get all items that are available

let availableItem = items.filter(values => values.available == true);
console.log(availableItem);

// 2. Get all items containing only Vitamin C.
let containingVitaminC = items.filter(values => values.contains == "Vitamin C");
console.log(containingVitaminC);

// 3. Get all items containing Vitamin A.
let containingVitaminA = items.filter(values => values.contains.includes("Vitamin A"))
console.log(containingVitaminA);

// 4. Group items based on the Vitamins that they contain in the following format:
//     {
//         "Vitamin C": ["Orange", "Mango"],
//         "Vitamin K": ["Mango"],
//     }

//     and so on for all items and all Vitamins.

let grouping = items.reduce((accumulator, currentValue) => {
     currentValue.contains.split(", ").map(vitaminContain => {
          if (accumulator[vitaminContain]) {
               accumulator[vitaminContain] =
                    accumulator[vitaminContain] + ", " + currentValue.name;
          } else {
               accumulator[vitaminContain] = currentValue.name;
          }
          return vitaminContain;
     });

     return accumulator;
}, {});
console.log(grouping);

// 5. Sort items based on number of Vitamins they contain.
let sortedData = items.sort((firstvalue, secondvalue) => {
     let itemNumber1 = firstvalue.contains.split(",").length;
     let itemNumber2 = secondvalue.contains.split(",").length;
     if (itemNumber1 > itemNumber2) {
          return -1;
     } else {
          return 1;
     }
});
console.log(sortedData);
